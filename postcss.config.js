module.exports = {
  'syntax': 'postcss-scss',
  plugins: [
    require('postcss-image-set-polyfill')({ /* ...options */ }),
    require('postcss-cssnext')({ /* ...options */ }),
    require('postcss-utilities')({ /* ...options */ }),
    require('rucksack-css')({ /* ...options */ }),
    require('postcss-at2x')({ /* ...options */ }),
    require('postcss-flexbox')({ /* ...options */ }),
    require('postcss-flexbugs-fixes')({ /* ...options */ }),
  ]
}