// Make Enzyme functions available in all test files without importing

// eslint-disable-next-line import/no-extraneous-dependencies
import { shallow, render, mount } from 'enzyme';

global.shallow = shallow;
global.render = render;
global.mount = mount;

// Fail tests on any warning
// eslint-disable-next-line no-console
console.error = (message) => {
  throw new Error(message);
};
