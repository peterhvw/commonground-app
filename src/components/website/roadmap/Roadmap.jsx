import React, { Component } from 'react';
import { connect } from 'react-redux';
import withSizes from 'react-sizes';
/* $FlowIssue - do import */
import * as actions from '~/src/actions';
import RoadPoints from './RoadPoints';
import RoadCard from './RoadCard';
import roadMapCards from './config';
import { getRoadPoint } from '../../../root-reducers';

// @flow

type Props = {
  roadPoint: number;
  width: number;
}

type State = {

}
  /* eslint-disable class-methods-use-this */

class Roadmap extends Component<Props, State> {
  componentDidMount(){}

  getWidth(width) {
    if (width > 900) {
      return '100%';
    }
    const inverse = 900 - width;
    const newWidth = 100 + (inverse / 3);

    return `${newWidth}%`;
  }

  render() {
    const { roadPoint, width } = this.props;
    const currentCard = roadMapCards[roadPoint - 1];
    return (
      <div id="roadmap" className="road-map">
        <h1>Roadmap</h1>
        <RoadPoints />
        <div className="road-card-c">
          <RoadCard {...currentCard} width={this.getWidth(width)} widthTrue={width} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state =>
  ({
    roadPoint: getRoadPoint(state),
  });

const mapSizesToProps = ({ width }) => ({
  width,
});

export default connect(mapStateToProps, actions)(withSizes(mapSizesToProps)(Roadmap));
