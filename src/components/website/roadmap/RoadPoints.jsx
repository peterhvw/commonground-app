import React, { Component } from 'react';
import { connect } from 'react-redux';
import Interactive from 'react-interactive';
import withSizes from 'react-sizes';
/* $FlowIssue - do import */
import { Colors } from '~/src/constants/constants';
/* $FlowIssue - do import */
import * as actions from '~/src/actions';
/* $FlowIssue - do import */
import { getRoadPoint } from '~/src/root-reducers';

// @flow

type Props = {
  isMobile: boolean;
  isTablet: boolean;
  width: number;
  setRoadMapPoint: () => {};
  roadPoint: number
}

type State = {
  roadPoint: number;
}

const points = [
  { id: 1, isSpecial: false },
  { id: 2, isSpecial: true },
  { id: 3, isSpecial: false },
  { id: 4, isSpecial: false },
];

class RoadPoints extends Component<Props, State> {

  setSelected(i) {
    const { setRoadMapPoint } = this.props;
    setRoadMapPoint(i);
  }
  getRadius(i): number {
    const { isMobile, isTablet, roadPoint } = this.props;

    // eslint-disable-next-line no-nested-ternary
    const getDevice = isMobile ? 35 : isTablet ? 50 : 100;
    const radius = roadPoint === i ? getDevice * 1.5 : getDevice;
    return radius;
  }

  getItemStyling(i: number, isSpecial: boolean) {
    const { width, roadPoint } = this.props;
    const left = width / 8;

    const styling = {
      width: `${this.getRadius(i)}px`,
      height: `${this.getRadius(i)}px`,
      border: `${roadPoint === i ? 4 : 2}px solid ${isSpecial ? Colors.brandPrimaryY : Colors.brandPrimaryO}`,
      backgroundColor: `${isSpecial ? Colors.brandSecondary : Colors.brandSecondaryLight}`,
      marginTop: `${roadPoint === i ? 0 : this.getRadius(i) / 4}px`,
      marginLeft: left - (this.getRadius(i) / 2),
    };

    return styling;
  }

  render() {
    return (
      <div className="road-points">
        {points.map(point =>
          (<div className="road-point" key={point.id}>
            <Interactive
              as="button"
              hover={{ backgroundColor: Colors.brandPrimaryO }}
              onClick={() => this.setSelected(point.id)}
              style={this.getItemStyling(point.id, point.isSpecial)}
            >
              <span>{point.id}</span>
            </Interactive>
          </div>
          ))
        }
      </div>
    );
  }
}

const mapSizesToProps = ({ width }) => ({
  isMobile: width < 480,
  isTablet: width < 1000 && width > 480,
  width,
});

const mapstateToProps = state => ({
  roadPoint: getRoadPoint(state),
})

export default connect(mapstateToProps, actions)(withSizes(mapSizesToProps)(RoadPoints));
