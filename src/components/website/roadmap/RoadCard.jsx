import React from 'react';

// @flow

type Props = {
  title: string;
  subTitle: string;
  img: string;
  text1: string;
  text2: string;
  alt: string;
}

const RoadCard = ({
  title, subTitle, img, text1, text2, alt
}: Props) => (
  <div className="road-card">
    <div className="road-info">
      <h1>{title}</h1>
      <h3>{subTitle}</h3>
      <p>{text1}</p>
      <p>{text2}</p>
    </div>
    <div className="img-overlay"></div>
      <div className="road-img-c">
        <img src={img} alt={alt} />
      </div>
  </div>
);



export default RoadCard;
