import React from 'react';

const Footer = () => (
  <div className="footer">
    <label>instagram: @commongroundgym</label>
    <label>info@thecommonground.nl</label>
    <label>facebook: @commongroundgym</label>
  </div>
);

export default Footer;
