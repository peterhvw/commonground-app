import React, { Component } from 'react';
import { IMGLesES, IMGLesSB, IMGLesAq } from '~/src/images';


export default class Partners extends Component {
  componentDidMount() {

  }
  render() {
    return (
      <div id="lessen">
        <div className="head-title">Sport Groups</div>
        <div className="lessen-img-c">
          <div className="group-c">
            <a href="http://www.silverbackz.nl/">Calisthenics - more info:</a>
            <img src={IMGLesSB} alt="silverbackz" />
          </div>
          <div className="group-c">
            <a href="http://www.elevatingsports.nl">Tricking - more info:</a>
            <img src={IMGLesES} alt="ElevatignSport" />
          </div>
          <div className="group-c">
            <a href="http://www.aquilaagilis.org">Parkour - more info:</a>
            <img src={IMGLesAq} alt="aquila agilis" />
          </div>
        </div>
      </div>
    );
  }
}
