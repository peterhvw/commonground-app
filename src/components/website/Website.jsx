import React, { Component } from 'react';

import Lessen from './lessen/Lessen';
import Navbar from './navbar/Navbar';
import HomePlayer from './home-player/Home-player';
import Visie from './visie/Visie';
import Partners from './partners/Partners';
import SportGroups from './sport-groups/Sport-groups';
import Roadmap from './roadmap/Roadmap';
import Footer from './footer/footer';

class Website extends Component {
  componentDidMount() {
  }

  render() {
    return (
      <div className="c">
        <Navbar />
        <HomePlayer />
        <Lessen />
        <SportGroups />
        <Partners />
        <Visie />
        <Roadmap />
        <Footer />
      </div>
    );
  }
}

export default Website;


// Sleeping Components
/*
import { Route } from 'react-router-dom';
import Commonground from './Commonground';
import Lessen from './Lessen';
import Navbar from './navbar/Navbar';
import HomePlayer from './home-player/Home-player';


    <Navbar />
    <HomePlayer />
    <CommingSoon />
    <Route exact path="/" component={Logo} />
    <Route path="/over" component={Over} />
    <Route path="/commonground" component={Commonground} />
    <Route path="/lessen" component={Lessen} />
*/
