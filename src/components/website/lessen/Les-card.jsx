import React, { Component } from 'react';

// @flow

type Props = {
  locatie: string,
  tijd: string,
  adres: string,
  trainers: string,
}

type State = {}

class LesCard extends Component<Props, State> {
  componentDidMount() {}

  render() {
    const {
      locatie, tijd, adres, trainers,
    } = this.props;
    return (
      <div className="les-card">
        <h5 className="title">{locatie}</h5>
        <div className="text">
          {tijd}
          <br />
          <h5>Adres</h5>
          {adres}
          <br />
          <h5>Trainers</h5>
          {trainers}
        </div>
      </div>
    );
  }
}

export default LesCard;
