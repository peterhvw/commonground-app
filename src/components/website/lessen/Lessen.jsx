import React from 'react';
import lessen from './config';
import LesCard from './Les-card';

const Lessen = () => (
  <div id="lessen">

    <div className="head-title">Lessen</div>
    <br />
    {
      lessen.map(les => (
        <LesCard key={les.adres} {...les} />
      ))
    }
  </div>
);

export default Lessen;
