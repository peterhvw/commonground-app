import React, { Component } from 'react';
import ReactSVG from 'react-svg';
import withSizes from 'react-sizes';
import { connect } from 'react-redux';
/* $FlowIssue - do import */
import { IMGlogo } from '~/src/images';
/* $FlowIssue - do import */
import { getMobileNavState } from '~/src/root-reducers';
/* $FlowIssue - do import */
import * as actions from '~/src/actions';
import Navbutton from './Navbutton';

// @flow
  /* eslint-disable class-methods-use-this */

class Navbar extends Component {
  mobileNav() {
    const { toggleButton, mobileNavState } = this.props;
    return (
      <div className="nav-mobile">
        <div id="nav-icon2" className={mobileNavState ? "open" : ""} onClick={() => toggleButton() }>
          <span />
          <span />
          <span />
          <span />
          <span />
          <span />
        </div>
        {
          mobileNavState ? this.desktopNav() : this.not()
        }
      </div>
    );
  }

  not() {
    return (
      <div> </div>
    )
  }

  desktopNav() {
  return (
    <div className="nav-desk">
      <Navbutton link="lessen">
        Lessen
      </Navbutton>
      <Navbutton link="visie">
        Common Core
      </Navbutton>
      <Navbutton link="roadmap">
        Roadmap
      </Navbutton>
    </div>
  );
  }



  render() {
    const { isMobile, mobileNavState } = this.props;
    return (
      <nav style={mobileNavState ? { height: 'unset' } : { height: '100px' }} className="navbar">
        <div className="navbar-logo-container">
          <ReactSVG
            path={IMGlogo}
            className="navbar-logo"
          />
        </div>
        {
          isMobile ? this.mobileNav() : this.desktopNav()
        }
      </nav>
    );
  }
}

const mapSizesToProps = ({ width }) => ({
  isMobile: width < 1000,
});

const mapStateToProps = state => ({
  mobileNavState: getMobileNavState(state),
});


export default connect(mapStateToProps, actions)(withSizes(mapSizesToProps)(Navbar));
