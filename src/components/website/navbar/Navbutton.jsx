import React from 'react';

// @flow
type State = {
  link: string,
  children: string,
}

const goToLink = (link) => {
  const goToElement = document.getElementById(`${link}`);
  window.scrollTo({
    top: goToElement.offsetTop,
    left: 0,
    behavior: 'smooth',
  });
};

const NavButton = ({ link, children }: State) => (
  <button
    className="nav-button"
    onClick={() => goToLink(link)}
  >
    {children}
  </button>
);

export default NavButton;
