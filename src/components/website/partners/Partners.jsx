import React, { Component } from 'react';
import { IMGPartnerRoset, IMGPartnerSBA } from '~/src/images';


export default class Partners extends Component {
  componentDidMount() {

  }
  render() {
    return (
      <div id="partners">
        <div className="title-c">
          <div className="parnter-title">Partners</div>
        </div>
        <div className="partners-img-c">
          <div className="partner-c">
            <a href="http://www.rozet.nl/">Rozet - more info:</a>
            <img src={IMGPartnerRoset} alt="rozet" />
          </div>
          <div className="partner-c">
            <a href="https://www.sportbedrijfarnhem.nl/"> more info:</a>
            <img style={ { marginTop: "20px", } } src={IMGPartnerSBA} alt="ElevatignSport" />
          </div>
        </div>
      </div>
    );
  }
}
