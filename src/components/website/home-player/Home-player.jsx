import React, { Component } from 'react';
import ReactPlayer from 'react-player';
import ReactSVG from 'react-svg';
import withSizes from 'react-sizes';
/* $FlowIssue - do import */
import { VIDEOhomeplayer } from '~/src/video';
/* $FlowIssue - do import */
import { IMGHomePlayer } from '~/src/images';

// @flow

type Props = {
  width: number,
}

type State = {
  isPlaying: boolean,
}

class HomePlayer extends Component< Props, State > {
  constructor(props, context) {
    super(props, context);
    this.state = { isPlaying: false };
  }

  /* eslint-disable class-methods-use-this */
  /* eslint-disable react/jsx-no-bind */

  getWidth(width) {
    if (width > 1600) {
      return '100%';
    }
    const inverse = 1600 - width;
    const newWidth = 100 + (inverse / 5);

    return `${newWidth}%`;
  }

  getTranslation(width) {
    if (width > 1400) {
      return 'translateX(0%)';
    }
    const inverse = 1400 - width;
    const newWidth = inverse / 18;

    return `translateX(-${newWidth}%)`;
  }

  playPause(): void {
    const { isPlaying } = this.state;
    this.setState({ isPlaying: !isPlaying });
  }

  playIcon() {
    const { isPlaying } = this.state;
    if (isPlaying) {
      return (
        <div>
        </div>
      );
    }

    return (
      <div
        className="button-c"
        onClick={() => this.playPause()}
      >
        <ReactSVG
          path={IMGHomePlayer}
          className="play-button"
        />
      </div>
    );
  }


  render() {
    const { isPlaying } = this.state;
    const { width } = this.props;
    return (
      <div className="player-c">
        <button className="overlay" onClick={this.playPause.bind(this)} />
        <div className="overlay-text-c">
          <div className="overlay-text">
              We are Urban Sport Arnhem
          </div>
        </div>
        {this.playIcon()}
        <div className="player-wrapper" style={{ width: this.getWidth(width), transform: this.getTranslation(width) }}>
          <ReactPlayer
            url={VIDEOhomeplayer}
            playing={isPlaying}
            loop={true}
            controls={false}
            volume={0}
            height="100%"
            width="100%"
            className="react-player"
          />
        </div>
      </div>
    );
  }
}

const mapSizesToProps = ({ width }) => ({
  width,
});

export default withSizes(mapSizesToProps)(HomePlayer);
