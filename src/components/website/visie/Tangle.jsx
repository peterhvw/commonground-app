import React from 'react';
// import ReactSVG from 'react-svg';
/* $FlowIssue - do import */
// import { IMGtangle } from '~/src/images';

// @flow

const Tangle = () => (
  <div className="tangle-c">
    <span className="tangle-text">
      Common Core
    </span>
    {/* <ReactSVG
      path={IMGtangle}
      className="tangle"
    /> */}
  </div>
);

export default Tangle;
