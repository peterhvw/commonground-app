import React, { Component } from 'react';
import { connect } from 'react-redux';
import withSizes from 'react-sizes';
import Swipeable from 'react-swipeable';
/* $FlowIssue - do import */
import * as actions from '~/src/actions';
import VisieCard from './Visie-card';
import cards from './config';
import Tangle from './Tangle';
import { getVisieCard } from '../../../root-reducers';

// @flow

type Props = {
  isMobile: boolean,
  isTablet: boolean,
  swipeVisieCard: () => {},
  visieCard: number,
}
type State = {
  imageIdx: number,
  visieCard: number,
}

const RIGHT = '-1';
const LEFT = '+1';

class Visie extends Component<Props, State> {
  componentDidMount(){}

  onSwiped(direction) {
    const { isTablet, swipeVisieCard, visieCard } = this.props;
    const change = direction === RIGHT ? RIGHT : LEFT;
    const adjustedIdx = visieCard + Number(change);
    let newIdx;
    const cardsLength = isTablet ? cards.length - 1 : cards.length;
    if (adjustedIdx >= cardsLength) {
      newIdx = 0;
    } else if (adjustedIdx < 0) {
      newIdx = cardsLength - 1;
    } else {
      newIdx = adjustedIdx;
    }
    swipeVisieCard(newIdx);
  }

  swipeButtons() {
    const { isTablet } = this.props;
    const buttonLeft = {
      left: isTablet ? '50%' : '0px',
    };
    const buttonRight = {
      right: '0px',
    };
    return (
      <div className="swipe-button">
        <button
          onClick={() => this.onSwiped(RIGHT)}
          style={buttonLeft}
        >
          ⇦
        </button>
        <button
          onClick={() => this.onSwiped(LEFT)}
          style={buttonRight}
        >
          ⇨
        </button>
      </div>
    );
  }

  mobileCards(visieCard: number) {
    const card = cards[visieCard];
    return (
      <Swipeable
        className="cards-mobile"
        trackMouse
        preventDefaultTouchmoveEvent
        onSwipedLeft={() => this.onSwiped(LEFT)}
        onSwipedRight={() => this.onSwiped(RIGHT)}
      >
        <VisieCard
          key={card.id}
          {...card}
        >
          {this.swipeButtons()}
        </VisieCard>
      </Swipeable>
    );
  }

  tabletCards(visieCard: number) {
    const [cardImportant, ...otherCards] = cards;
    const card = otherCards[visieCard];
    return (
      <div className="cards-tablet-c">
        <VisieCard
          className="card-tablet"
          {...cardImportant}
        />
        <Swipeable
          className="cards-tablet"
          trackMouse
          preventDefaultTouchmoveEvent
          onSwipedLeft={() => this.onSwiped(LEFT)}
          onSwipedRight={() => this.onSwiped(RIGHT)}
        >
          <VisieCard
            key={card.id}
            {...card}
          >
            {this.swipeButtons()}
          </VisieCard>
        </Swipeable>
      </div>
    );
  }
  /* eslint-disable class-methods-use-this */
  desktopCards() {
    return (
      <div className="cards-desktop">
        {cards.map(card =>
          (<VisieCard
            key={card.id}
            {...card}
          />
          ))
        }
      </div>);
  }

  render() {
    const { isMobile, isTablet, visieCard, width } = this.props;
    return (
      <div id="visie">
        <Tangle />
        <div className="cards-c">
          {
            // eslint-disable-next-line no-nested-ternary
            isMobile ? this.mobileCards(visieCard) : isTablet ? this.tabletCards(visieCard) : this.desktopCards()
          }
        </div>
      </div>
    );
  }
}

const mapSizesToProps = ({ width }) => ({
  isMobile: width < 1000,
  isTablet: width < 1200 && width > 1000,
  width,
});

const mapStateToProps = state => ({
  visieCard: getVisieCard(state),
});

export default connect(mapStateToProps, actions)(withSizes(mapSizesToProps)(Visie));
