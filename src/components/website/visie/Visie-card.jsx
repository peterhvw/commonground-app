import React, { Component } from 'react';

// @flow

type Props = {
  title: string,
  text: string,
  points: string[],
  children: any,
}

type State = {}

class VisieCard extends Component<Props, State> {
  componentDidMount() {}

  render() {
    const {
      title, text, points, children,
    } = this.props;
    return (
      <div className="visie-card">
        { children }
        <div className="title">
          <h2>{title}</h2>
        </div>
        <div className="text">
          {text}
        </div>
        <div className="list">
          <ul>
            { points.map(point =>
              <li key={point}>{point}</li>)
            }
          </ul>
        </div>
      </div>
    );
  }
}

export default VisieCard;
