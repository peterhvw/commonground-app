import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../actions';

// @flow
interface Props {
  CommonGround: () => any
}
interface State {}

class CommonGround extends Component< Props, State > {
  componentDidMount() {
  }

  render() {
    return (
      <div>
        <h2>
        Common Ground
        </h2>
        <h4>
        Waarden Common Ground
        </h4>
        <p>
        - Creativiteit, vrijheid en zelfexpressie
        - Persoonlijk ontwikkeling
        - Samenwerking en co-creatie (synergie)
        - Community vorming
        </p>
        <h4>
        De locatie gaat onderdak gaan bieden aan:
        </h4>
        <p>
        Avonduren:
        - Lessen in tricking, freerunning, calisthenics
        - Vrije trainingen
        Ochtend en middag:
        - Creatieve en sportieve flexwerkplekken (Common Office)
        - Bootcamps van personal trainers
        - Bedrijfjes (denk aan gezonde voeding, fysiotherapie, bedrijfstrainingen/workshops)
        - Wijkprogramma's (kinderen op gezond gewicht etc..)
        - Enz.
        Overig.
        - Urban sportevenementen en "gatherings"
        - Projectmatig werken (Zoals het project "Urban playground" )
        *De core-business blijft echter voorlopig de lessen en vrije trainingen.
        </p>
        <h4>
        Project "Urban Playground"
        </h4>
        <p>
        Binnen de stads ontwikkeling willen wij Urban Sports een prominentere rol geven in het straatbeeld. Wij zijn van mening dat urban sports toegankelijker moet worden voor jong en oud. 
        Wij willen gemeentelijk beleid sturen op dit vlak en bieden waarden op het vlak van consultancy (het vertalen van behoeftes van mensen), parkontwerp en projectmanagement.
        Locatie eisen en wensen
        Eisen:
        - minstens 250m2
        - een hoog plafond v.a. 4/5 meter
        Wensen
        - Dit mag van alles zijn (leegstaand erfgoed, loodsen, gymzalen, fabriekshallen, schuren)
        - Kleedruimtes
        - De locatie kan naar eigen wens aangepast worden tbv de sporten
        - Afgesloten kantoorruimte
        </p>
      </div>
    );
  }
}

// eslint-disable-next-line no-class-assign
CommonGround = withRouter(connect(null, actions)(CommonGround));

export default CommonGround;
