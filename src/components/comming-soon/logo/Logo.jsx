import React from 'react';
import ReactSVG from 'react-svg';
/* $FlowIssue - do import */
import { IMGlogo } from '~/src/images';

// @flow

const Logo = () => (
  <div>
    <ReactSVG
      path={IMGlogo}
      className="logo"
    />
  </div>
);

export default Logo;
