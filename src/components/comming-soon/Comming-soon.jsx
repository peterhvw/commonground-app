import React from 'react';
import Logo from './logo/Logo';

const CommingSoon = () => (
  <div className="comming-soon c">
    <h1>Common Soon</h1>
    <Logo />
    <h1>info@thecommonground.nl</h1>
  </div>
);

export default CommingSoon;
