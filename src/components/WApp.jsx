import React from 'react';
import { Route } from 'react-router-dom';
// import App from './app/App';
import Website from './website/Website';

const WApp = () => (
  <div className="c">
    <Route exact path="/" component={Website} />
  </div>
);

export default WApp;
