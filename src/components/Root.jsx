import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';

import WApp from './WApp';

// @flow
export type Store = {
  store: any,
  history: any,
}

const Root = ({ store, history }: Store) => (
  <Provider store={store}>
    <Router history={history}>
      <WApp />
    </Router>
  </Provider>
);

export default Root;
