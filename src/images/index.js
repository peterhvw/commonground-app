const pathToLessen = require.context('./lessen', true);
const lessen = [
  './IMG_1186.JPG',
  './silverbackz.jpg',
  './ag.jpg',
];
lessen.map(img => pathToLessen(img, true));

const pathToPartners = require.context('./partners', true);
const partners = [
  './rozetlogo.jpg',
  './sportbedrijflogo.png',
];
partners.map(img => pathToPartners(img, true));

const pathTohomePlayer = require.context('./homePlayer', true);
const homePlayer = [
  './playicon.svg',
];
homePlayer.map(img => pathTohomePlayer(img, true));

const pathToBase = require.context('./base', true);
const base = [
  './logo.svg',
  './tangle.svg',
];
base.map(img => pathToBase(img, true));

const pathToVisie = require.context('./visie', true);
const visie = [
  './IMG_0767.jpg',
  './CommonGround.jpg',
  './IMG_2018.jpg',
  './koepelrender.jpg',
];
visie.map(img => pathToVisie(img, true));

const takeName = (img) => {
  const parts = img.split('/');
  const lastItem = parts.length;
  return `public/images/${parts[lastItem - 1]}`;
};

export const IMGvisie1 = takeName(visie[0]);
export const IMGvisie2 = takeName(visie[1]);
export const IMGvisie3 = takeName(visie[2]);
export const IMGvisie4 = takeName(visie[3]);
export const IMGlogo = takeName(base[0]);
export const IMGtangle = takeName(base[1]);
export const IMGLesES = takeName(lessen[0]);
export const IMGLesSB = takeName(lessen[1]);
export const IMGLesAq = takeName(lessen[2]);
export const IMGPartnerRoset = takeName(partners[0]);
export const IMGPartnerSBA = takeName(partners[1]);
export const IMGHomePlayer = takeName(homePlayer[0]);
