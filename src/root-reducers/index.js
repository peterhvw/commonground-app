import { combineReducers } from 'redux';

const roadMapPoint = (state = 2, action) => {
  switch (action.type) {
    case 'SETROADPOINT':
      return action.payload;
    default:
      return state;
  }
};
const visieCard = (state = 0, action) => {
  switch (action.type) {
    case 'SWIPE_VISIE_CARD':
      return action.payload;
    default:
      return state;
  }
};
const mobileNav = (state = false, action) => {
  switch (action.type) {
    case 'TOGGLE_MOBILE_NAV':
      return !state;
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  roadMapPoint,
  visieCard,
  mobileNav,
});

export const getRoadPoint = state => state.roadMapPoint;
export const getVisieCard = state => state.visieCard;
export const getMobileNavState = state => state.mobileNav;

export default rootReducer;
