export const Colors = {
  brandSecondary: '#2d3c53',
  brandSecondaryLight: '#3f5474',
  brandPrimaryO: '#ff8800',
  brandPrimaryY: '#f6ca19',
};

export const Names = {
  commonground: 'Common Ground',
};
