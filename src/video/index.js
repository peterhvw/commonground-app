const pathTohomeplayer = require.context('./homeplayer', true);
const homeplayer = [
  './video1.mp4',
];
homeplayer.map(video => pathTohomeplayer(video, true));

const takeName = (video) => {
  const parts = video.split('/');
  const lastItem = parts.length;
  return `public/video/${parts[lastItem - 1]}`;
};

export const VIDEOhomeplayer = takeName(homeplayer[0]);
