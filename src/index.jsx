import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { configureStore, history } from './configureStore';
import './images';
import './style/styles.scss';
import Root from './components/Root';

/* eslint-disable global-require */
const store = configureStore();

const renderRoot = (app) => {
  // eslint-disable-next-line no-undef
  render(app, document.getElementById('root'));
};

if (process.env.NODE_ENV === 'production') {
  renderRoot((
    <Root store={store} history={history} />
  ));
} else {
  renderRoot((
    <AppContainer>
      <Root store={store} history={history} />
    </AppContainer>
  ));

  if (module.hot) {
    module.hot.accept('./components/Root', () => {
      const newStore = require('./configureStore').configureStore();
      const hotStore = newStore.store;
      const hotHistory = newStore.history;
      const HotApp = require('./components/Root');
      renderRoot((
        <AppContainer>
          <HotApp store={hotStore} history={hotHistory} />
        </AppContainer>
      ));
    });
    module.hot.accept('./root-reducers', async () => {
      const newRootReducer = await require('./root-reducers');
      store.replaceReducer(newRootReducer);
    });
  }
}
/* eslint-enable global-require */
