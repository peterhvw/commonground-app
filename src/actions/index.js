// import * as api from '../api';

// @flow

export const setRoadMapPoint = (point: number) => dispatch => {
  dispatch({
    type: 'SETROADPOINT',
    payload: point,
  })
}

export const swipeVisieCard = (card: number) => dispatch => {
  dispatch({
    type: 'SWIPE_VISIE_CARD',
    payload: card
  })
}

export const toggleButton = () => dispatch => {
  dispatch({
    type: 'TOGGLE_MOBILE_NAV',
  })
}