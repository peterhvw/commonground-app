var HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
var path = require("path");
const webpack = require('webpack');

var isProduction = false;
if (process.env.NODE_ENV === 'production') {
  isProduction = true;
}  

module.exports = {
  entry: {
    'app': [
      'babel-polyfill',
      'react-hot-loader/patch',
      './src/index.jsx'
    ]
  },
  output: {
    path: isProduction ? path.resolve(__dirname) : path.resolve(__dirname, "public"),
    filename: isProduction ? "public/js/[name].[hash].bundle.js" : "js/[name].bundle.js"
  },
  devServer: {
    compress: false,
    port: 8080,
    hot: true,
    historyApiFallback: true
  },
  devtool: isProduction ? '(none)': "cheap-module-source-map",
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: isProduction ? 
        ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'postcss-loader', 'sass-loader']
        })
        :
        [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'postcss-loader' },
          { loader: 'sass-loader' }
        ]
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.mp4$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: isProduction ? 'public/video/' : 'video/'
            }
          }
        ],
        exclude: /node_modules/,
        include: __dirname,
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: isProduction ? 'public/images/' : 'images/'
            }
          }
        ],
        exclude: /node_modules/,
        include: __dirname,
      },
    ]
  },
  resolve: {
    extensions: [".js", ".jsx"],
  },
  plugins: isProduction ? [
    new ExtractTextPlugin({
      filename: isProduction? 'public/style/app.[hash].css' : './style/app.css',
      disable: false,
      allChunks: true
    }),
    new HtmlWebpackPlugin({
      title: 'CommonGround',
      minify: {
        collapseWhitespace: true
      },
      hash: false,
      Chunks: ['app'],
      filename: 'public/index.html',
      template: './src/index.html',
    }),
  ]
    :
    [
    new HtmlWebpackPlugin({
      title: 'CommonGround',
      minify: {
        collapseWhitespace: true
      },
      hash: false,
      Chunks: ['app'],
      filename: './index.html',
      template: './src/index.html',
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    ]
}
